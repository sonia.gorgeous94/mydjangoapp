from django.http import HttpResponse
from django.template import loader


def index(request):
    template = loader.get_template('index.html')
    context = {
        'name': 'Sonia',
        'fname': 'Khaled',
        'course': 'Python Django',
        'name2': 'Greg',
        'fname2': 'Wilson',
        'course2': 'Python Django',
    }
    return HttpResponse(template.render(context,  request))

